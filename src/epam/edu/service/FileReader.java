package epam.edu.service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import epam.edu.model.User;

public class FileReader {

	private static List<User> users = new ArrayList<User>();

	public static List<User> readFromFile(String fileName) {

		try (Stream<String> stream = Files.lines(Paths.get(fileName))) {
			stream.forEach(item -> parseTextFileToUserCollection(item));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return users;
	}

	private static void parseTextFileToUserCollection(String line) {
		User user = new User();
		user.setId(Integer.valueOf(parseItem(line, "id")));
		user.setName(parseItem(line, "name"));
		user.setAge(Integer.valueOf(parseItem(line, "age")));
		user.setSalary(Integer.valueOf(parseItem(line, "salary")));
		users.add(user);

	}

	private static String parseItem(String line, String itemToFind) {

		String matchedItem = null;
		Pattern pattern = Pattern.compile(itemToFind + ": (\\w+);");
		Matcher matcher = pattern.matcher(line);
		while (matcher.find()) {
			matchedItem = matcher.group(1);
		}
		return matchedItem;
	}

	public static List<User> getUsers() {
		return users;
	}

}
