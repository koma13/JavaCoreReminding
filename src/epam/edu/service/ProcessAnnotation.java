package epam.edu.service;

import java.lang.reflect.Field;

import org.apache.log4j.Logger;

import epam.edu.model.FieldAnnotation;
import epam.edu.model.User;

public class ProcessAnnotation {

	private final static Logger LOGGER = Logger.getLogger(ProcessAnnotation.class.getName());

	private static final String SALARY_FIELD = "salary";
	private static final String AGE_FIELD = "age";
	private static final String NAME_FIELD = "name";
	private static final String ID_FIELD = "id";
	

	public static User inject(String lineFromFile) throws IllegalArgumentException, IllegalAccessException {
		User user = new User();
		Field[] fields = user.getClass().getDeclaredFields();
		for (Field field : fields) {
			if (field.isAnnotationPresent(FieldAnnotation.class)) {
				field.setAccessible(true);
				String fieldName = field.getName();

				switch (fieldName) {
				case ID_FIELD:
					field.set(user, FileReaderUtils.parseItemInt(lineFromFile, fieldName));
					break;
				case NAME_FIELD:
					field.set(user, FileReaderUtils.parseItem(lineFromFile, fieldName));
					break;
				case AGE_FIELD:
					field.set(user, FileReaderUtils.parseItemInt(lineFromFile, fieldName));
					break;
				case SALARY_FIELD:
					field.set(user, FileReaderUtils.parseItemInt(lineFromFile, fieldName));
					break;
				}
			}
		}
		return user;

	}

	public static Integer getIntFieldValue(String fieldName, User user) {
		Field field;
		Integer fieldValue = null;
		try {
			field = user.getClass().getDeclaredField(fieldName);
			field.setAccessible(true);
			fieldValue = (Integer) field.get(user);
		} catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {
			LOGGER.error("Error occurs when trying get " + fieldName + " field value of " + user.getClass().getName() + " class");
		}
		return fieldValue;
	}

}
