package epam.edu.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.List;

import org.apache.log4j.Logger;

import epam.edu.helper.UserCollection;
import epam.edu.model.User;

public class Deserializer implements Runnable {

	public static List<User> deserializedUsers = new UserCollection<User>();

	public Deserializer(String folderName) {
		this.folderName = "C://" + folderName;
	}

	private final static Logger LOGGER = Logger.getLogger(Deserializer.class.getName());
	String folderName;

	@Override
	public void run() {
		synchronized (User.class) {
			User user = new User();
			for (final File fileEntry : new File(folderName).listFiles()) {
				try (FileInputStream fileInputStream = new FileInputStream(fileEntry);
						ObjectInputStream inputStream = new ObjectInputStream(fileInputStream)) {
					user = (User) inputStream.readObject();
					deserializedUsers.add(user);
				} catch (IOException e) {
					LOGGER.error("There is a problem reading from file " + fileEntry + " error message: " + e);
				} catch (ClassNotFoundException e) {
					LOGGER.error("Error occures when try cast readed object to" + user.getClass().getName() + "class ", e);
				}
			}
		}
	}
}
