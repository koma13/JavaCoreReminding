package epam.edu.service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.util.List;

import org.apache.log4j.Logger;

import epam.edu.model.User;

public class Serializer implements Runnable {

	private final static Logger LOGGER = Logger.getLogger(Serializer.class.getName());

	private final static String lowSalaryDirectoryName = "C:\\low_salary";
	private final static String highSalaryDirectoryName = "C:\\high_salary";
	private static final String INPUT_FILE_NAME = "aggregates.txt";
	private static final String SALARY_FIELD = "salary";

	@Override
	public void run() {
		serializeToFiles();
	}

	private void setUp() {
		
		createDirectoryIfNotExist(lowSalaryDirectoryName);
		createDirectoryIfNotExist(highSalaryDirectoryName);
	}

	private void serializeToFiles() {
		setUp();
		List<User> users = FileReaderUtils.readFromFile(INPUT_FILE_NAME);
		for (User user : users) {
			boolean isSalaryHigh = false;
			Integer salaryValue = ProcessAnnotation.getIntFieldValue(SALARY_FIELD, user);
			if (salaryValue >= 2000)
				isSalaryHigh = true;
			Serializer.serializeUser(user, isSalaryHigh);
		}
	}

	public static void serializeUser(User user, boolean isSalaryHigh) {
		int userId = ProcessAnnotation.getIntFieldValue("id", user);
		String fileName;
		if (isSalaryHigh)
			fileName = highSalaryDirectoryName + "/user" + userId + ".ser";
		else
			fileName = lowSalaryDirectoryName + "/user" + userId + ".ser";
		try (FileOutputStream fileOutputStream = new FileOutputStream(new File(fileName));
				ObjectOutputStream outputStream = new ObjectOutputStream(fileOutputStream);) {
			outputStream.writeObject(user);
		} catch (Exception e) {
			LOGGER.error("There is a problem reading from file " + fileName + " error message: " + e);
		}
	}

	private static void createDirectoryIfNotExist(String directoryName) {
		File fileName = new File(directoryName);
		if (!fileName.exists()) {
			fileName.mkdir();
		} else
			for (File file : fileName.listFiles()) {
				file.delete();
			}
	}

}
