package epam.edu.service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import org.apache.log4j.Logger;

import epam.edu.helper.UserCollection;
import epam.edu.model.User;

public class FileReaderUtils {

	private static final String FIELD_VALUE_REGEX = ": (\\w+);";
	public static final Logger LOGGER = Logger.getLogger(FileReaderUtils.class.getName());

	private static  List<User> users = new UserCollection<User>();

	public static  List<User> readFromFile(String fileName) {
		try (Stream<String> stream = Files.lines(Paths.get(fileName))) {
			stream.forEach(item -> parseTextFileToUserCollection(item));
		} catch (IOException e) {
			LOGGER.error("Error occurs while reading from file " + fileName, e);
			;
		}
		return users;
	}

	private static  void parseTextFileToUserCollection(String line) {
		User user = null;
		try {
			user = ProcessAnnotation.inject(line);
		} catch (IllegalArgumentException | IllegalAccessException e) {
			LOGGER.error("Error occurs while trying ", e);
		}
		users.add(user);

	}

	public static String parseItem(String line, String fieldName) {

		String matchedItem = null;
		Pattern pattern = Pattern.compile(fieldName + FIELD_VALUE_REGEX);
		Matcher matcher = pattern.matcher(line);
		while (matcher.find()) {
			matchedItem = matcher.group(1);
		}
		return matchedItem;
	}

	public static Integer parseItemInt(String line, String fieldName) {
		String matchedItem = parseItem(line, fieldName);
		return Integer.valueOf(matchedItem);
	}

	public  List<User> getUsers() {
		return users;
	}

}
