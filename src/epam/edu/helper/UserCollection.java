package epam.edu.helper;

import java.util.Arrays;
import java.util.Collection;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.function.Consumer;

public class UserCollection<T> implements List<T> {

	private int size = 0;
	transient Object[] elementData = {};
	private static final int INITIAL_CAPACITY = 10;
	protected transient int modCount = 0;

	public UserCollection() {
		elementData = new Object[INITIAL_CAPACITY];
	}

	@Override
	public Object[] toArray() {
		return Arrays.copyOf(elementData, size);
	}

	@Override
	public boolean add(Object e) {
		if (size == elementData.length) {
			ensureCapacity(); 
		}
		elementData[size++] = e;
		return true;
	}

	@Override
	public Object set(int index, Object element) {
		rangeCheck(index);

		T oldValue = elementData(index);
		elementData[index] = element;
		return oldValue;
	}

	@Override
	public ListIterator<T> listIterator() {
		return new ListItr(0);
	}

	@Override
	public ListIterator<T> listIterator(int index) {
		rangeCheckForAdd(index);

		return new ListItr(index);
	}

	private void ensureCapacity() {
		int newIncreasedCapacity = elementData.length * 2;
		elementData = Arrays.copyOf(elementData, newIncreasedCapacity);
	}

	@Override
	public List<T> subList(int fromIndex, int toIndex) {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	@Override
	public Iterator<T> iterator() {

		Iterator<T> it = new Iterator<T>() {

			private int currentIndex = 0;

			@Override
			public boolean hasNext() {
				return currentIndex < size && elementData[currentIndex] != null;
			}

			@Override
			public T next() {
				return (T) elementData[currentIndex++];
			}

			@Override
			public void remove() {
				throw new UnsupportedOperationException();
			}
		};
		return it;
	}

	private class Itr implements Iterator<T> {
		int cursor; // index of next element to return
		int lastRet = -1; // index of last element returned; -1 if no such
		int expectedModCount = modCount;

		public boolean hasNext() {
			return cursor != size;
		}

		@SuppressWarnings("unchecked")
		public T next() {
			checkForComodification();
			int i = cursor;
			if (i >= size)
				throw new NoSuchElementException();
			Object[] elementData = UserCollection.this.elementData;
			if (i >= elementData.length)
				throw new ConcurrentModificationException();
			cursor = i + 1;
			return (T) elementData[lastRet = i];
		}

		public void remove() {
			if (lastRet < 0)
				throw new IllegalStateException();
			checkForComodification();

			try {
				UserCollection.this.remove(lastRet);
				cursor = lastRet;
				lastRet = -1;
				expectedModCount = modCount;
			} catch (IndexOutOfBoundsException ex) {
				throw new ConcurrentModificationException();
			}
		}

		@SuppressWarnings("unchecked")
		public void forEachRemaining(Consumer<? super T> consumer) {
			Objects.requireNonNull(consumer);
			final int size = UserCollection.this.size;
			int i = cursor;
			if (i >= size) {
				return;
			}
			final Object[] elementData = UserCollection.this.elementData;
			if (i >= elementData.length) {
				throw new ConcurrentModificationException();
			}
			while (i != size && modCount == expectedModCount) {
				consumer.accept((T) elementData[i++]);
			}
			// update once at end of iteration to reduce heap write traffic
			cursor = i;
			lastRet = i - 1;
			checkForComodification();
		}

		final void checkForComodification() {
			if (modCount != expectedModCount)
				throw new ConcurrentModificationException();
		}
	}

	private class ListItr extends Itr implements ListIterator<T> {
		ListItr(int index) {
			super();
			cursor = index;
		}

		public boolean hasPrevious() {
			return cursor != 0;
		}

		public int nextIndex() {
			return cursor;
		}

		public int previousIndex() {
			return cursor - 1;
		}

		@SuppressWarnings("unchecked")
		public T previous() {
			checkForComodification();
			int i = cursor - 1;
			if (i < 0)
				throw new NoSuchElementException();
			Object[] elementData = UserCollection.this.elementData;
			if (i >= elementData.length)
				throw new ConcurrentModificationException();
			cursor = i;
			return (T) elementData[lastRet = i];
		}

		public void set(T e) {
			if (lastRet < 0)
				throw new IllegalStateException();
			checkForComodification();

			try {
				UserCollection.this.set(lastRet, e);
			} catch (IndexOutOfBoundsException ex) {
				throw new ConcurrentModificationException();
			}
		}

		public void add(T e) {
			checkForComodification();

			try {
				int i = cursor;
				UserCollection.this.add(i, e);
				cursor = i + 1;
				lastRet = -1;
				expectedModCount = modCount;
			} catch (IndexOutOfBoundsException ex) {
				throw new ConcurrentModificationException();
			}
		}

	}

	private void rangeCheck(int index) {
		if (index >= size)
			throw new IndexOutOfBoundsException("in index " + index);
	}

	private void rangeCheckForAdd(int index) {
		if (index < 0 || index > this.size)
			throw new IndexOutOfBoundsException(" in index " + index);
	}

	@SuppressWarnings("unchecked")
	T elementData(int index) {
		return (T) elementData[index];
	}

	@Override
	public int size() {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	@Override
	public boolean isEmpty() {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	@Override
	public boolean contains(Object o) {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	@Override
	public Object[] toArray(Object[] a) {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	@Override
	public boolean remove(Object o) {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	@Override
	public boolean containsAll(Collection c) {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	@Override
	public boolean addAll(Collection c) {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	@Override
	public boolean addAll(int index, Collection c) {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	@Override
	public boolean removeAll(Collection c) {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	@Override
	public boolean retainAll(Collection c) {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	@Override
	public void clear() {
		throw new UnsupportedOperationException("Not supported yet.");

	}

	@Override
	public T get(int index) {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	@Override
	public void add(int index, Object element) {
		throw new UnsupportedOperationException("Not supported yet.");

	}

	@Override
	public T remove(int index) {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	@Override
	public int indexOf(Object o) {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	@Override
	public int lastIndexOf(Object o) {
		throw new UnsupportedOperationException("Not supported yet.");
	}

}
