package epam.edu.model;

public @interface LineField {

	String value();

}
