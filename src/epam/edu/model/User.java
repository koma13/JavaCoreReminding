package epam.edu.model;

import java.io.Serializable;

public class User implements Serializable, Comparable<User> {

	private static final long serialVersionUID = -5627990886222486140L;

	@FieldAnnotation("{id}")
	private Integer id;
	@FieldAnnotation("{name}")
	private String name;
	@FieldAnnotation("{age}")
	private Integer age;
	@FieldAnnotation("{salary}")
	private Integer salary;

	@Override
	public String toString() {
	
		return "User [id=" + id + ", name=" + name + ", age=" + age + ", salary=" + salary + "]";
	}

	@Override
	public int compareTo(User o) {
		Integer age1 = this.age;
		Integer age2 = o.age;
		return age1.compareTo(age2);
	}

}
