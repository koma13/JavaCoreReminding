package epam.edu.main;

import java.util.Collections;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import epam.edu.model.User;
import epam.edu.service.Deserializer;
import epam.edu.service.Serializer;

public class Main {

	private static final String HIGH_SALARY_FOLDER_NAME = "high_salary";
	private static final String LOW_SALARY_FOLDER_NAME = "low_salary";

	public static void main(String[] args) throws InterruptedException {
		Thread thread0 = new Thread(new Serializer());
		thread0.run();
		thread0.join();

		ExecutorService threadPool = Executors.newFixedThreadPool(2);
		threadPool.execute(new Thread(new Deserializer(LOW_SALARY_FOLDER_NAME)));
		threadPool.execute(new Thread(new Deserializer(HIGH_SALARY_FOLDER_NAME)));

		threadPool.shutdown();
		while (!threadPool.isTerminated()) {
			// wait for all tasks to finish
		}
		printSortedByAgeUsers();
	}

	private static void printSortedByAgeUsers() {
		Collections.sort(Deserializer.deserializedUsers);
		for (User user : Deserializer.deserializedUsers) {
			System.out.println(user.toString());
		}
	}
}
